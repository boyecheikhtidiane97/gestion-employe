package com.uadb.springboot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EmployeNotFoundExeption extends RuntimeException {

	public EmployeNotFoundExeption(String mess) {
		super(mess);
		// TODO Auto-generated constructor stub
	}
	
	
	

}
