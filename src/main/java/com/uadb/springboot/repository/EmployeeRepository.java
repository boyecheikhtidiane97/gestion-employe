package com.uadb.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uadb.springboot.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
