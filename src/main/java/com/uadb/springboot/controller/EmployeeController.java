package com.uadb.springboot.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.uadb.springboot.exception.EmployeNotFoundExeption;
import com.uadb.springboot.model.Employee;
import com.uadb.springboot.repository.EmployeeRepository;

@RestController
public class EmployeeController {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@PostMapping("/employees")
	public Employee add(@RequestBody Employee employee) {
		
		return employeeRepository.save(employee);
	}
	
	
	  @GetMapping("/employees") public List<Employee> readAllEmploye(){
	  
	  return employeeRepository.findAll(); }
	  
	  @GetMapping("/employees/{id}")
	  public Employee readOneEmployee(@PathVariable Long id){
		  
		  Employee emp= employeeRepository.findById(id).orElseThrow(()->new EmployeNotFoundExeption("employee pas trouve"));
		  
		  return emp;
	  }
	  
	  @PutMapping("/employees/{id}")
	  public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee,@PathVariable Long id){
		  
		  employeeRepository.findById(id).orElseThrow(()->new EmployeNotFoundExeption("employee pas trouve"));
		  
		  Employee emp= employeeRepository.save(employee);
		  HttpHeaders headers= new HttpHeaders();
		  headers.add("token","123");
		  
		  return ResponseEntity.ok().headers(headers).body(emp);
	  }
	  
	  @DeleteMapping("employees/{id}")
	  public ResponseEntity<Map<String, Boolean>> deleteEmployee(@PathVariable Long id) {
		  
		  employeeRepository.findById(id).orElseThrow(()->new EmployeNotFoundExeption("employee pas trouve"));
		  employeeRepository.deleteById(id);
		  
		  Map<String,Boolean> response =new HashMap<>();
		  response.put("deleted",Boolean.TRUE);
		  
		  return ResponseEntity.ok(response);
	  }
	 

}
