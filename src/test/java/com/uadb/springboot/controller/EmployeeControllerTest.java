package com.uadb.springboot.controller;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.uadb.springboot.repository.EmployeeRepository;

@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureMockMvc
public class EmployeeControllerTest {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private MockMvc mockMvc;
	
	@BeforeAll
	@AfterAll
	public void cleanDB() {
		
		employeeRepository.deleteAll();
	}
	
	@Test
	public void TestCrer() {
		
	}

}
